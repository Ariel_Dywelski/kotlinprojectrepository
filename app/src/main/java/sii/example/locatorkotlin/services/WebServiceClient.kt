package sii.example.locatorkotlin.services

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST
import sii.example.locatorkotlin.dtos.DeviceDTO
import sii.example.locatorkotlin.dtos.DeviceLocationDTO



/**
 * Created by ariel_dywelski on 06/10/2017.
 */
interface WebServiceClient {

    @Headers("authorization: Basic c2lpcGhvbmVsb2NhdG9yOkN5c3RlTWllc3RvQCkhJg==")
    @GET("/locations/getAllWithDevices")
    fun getAuthorization(): Call<Void>

    @Headers("authorization: Basic c2lpcGhvbmVsb2NhdG9yOkN5c3RlTWllc3RvQCkhJg==")
    @POST("/devices/register")
    fun addDevice(@Body device: DeviceDTO): Call<Void>

    @Headers("authorization: Basic c2lpcGhvbmVsb2NhdG9yOkN5c3RlTWllc3RvQCkhJg==")
    @POST("/locations/save")
    fun deviceLocationStatus(@Body locationStatus: List<DeviceLocationDTO>): Call<Void>
}