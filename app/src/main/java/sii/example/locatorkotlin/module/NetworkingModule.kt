package sii.example.locatorkotlin.module

import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import sii.example.locatorkotlin.services.WebServiceClient
import javax.inject.Singleton



/**
 * Created by ariel_dywelski on 06/10/2017.
 */
@Module
class NetworkingModule {

    val BASE_URL: String = "http://siiphonelocator-webapi.azurewebsites.net"

    @Singleton
    @Provides
    fun provideGson(): Gson {
        return Gson()
    }

    @Singleton
    @Provides
    fun provideOkHttpClient(): OkHttpClient{
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val builder = OkHttpClient.Builder()
        builder.addInterceptor(httpLoggingInterceptor)

        return builder.build()
    }

    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient) : Retrofit{

        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build()
    }

    @Singleton
    @Provides
    fun provideWebService(retrofit: Retrofit): WebServiceClient {
        return retrofit.create(WebServiceClient::class.java)
    }
}