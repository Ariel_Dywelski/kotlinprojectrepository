package sii.example.locatorkotlin.location.beacons

import android.graphics.PointF
import com.estimote.sdk.Utils
import com.estimote.sdk.connection.scanner.ConfigurableDevicesScanner
import org.apache.commons.math3.fitting.leastsquares.LevenbergMarquardtOptimizer
import org.jetbrains.annotations.Nullable
import org.joda.time.DateTime
import sii.example.locatorkotlin.BeaconDeviceInfo
import sii.example.locatorkotlin.location.LocationProviderType
import sii.example.locatorkotlin.location.LocationRecord
import sii.example.locatorkotlin.trilateration.NonLinearLeastSquaresSolver
import sii.example.locatorkotlin.trilateration.TrilaterationFunction


/**
 * Created by ariel_dywelski on 09/10/2017.
 */
open class BeaconHelper {

    val MEASUREMENT_COUNT: Int = 5
    val LOCATION_BEACON_ID: String = "LocationBeacon"


    fun calculateDevicePosition(beacons: MutableMap<String, BeaconDeviceInfo>) : LocationRecord {
        val positions = Array(beacons.size) { DoubleArray(2) }
        val distance = DoubleArray(beacons.size)
        for ((i, beaconEntry) in beacons.entries.withIndex()) {
            val beacon = beaconEntry.value
            positions[i][0] = beacons[beacon.id]!!.getX()
            positions[i][1] = beacons[beacon.id]!!.getY()
            distance[i] = beacon.getElementsSum() / beacon.getCount()
        }
        val squaresSolver = NonLinearLeastSquaresSolver(TrilaterationFunction(positions, distance), LevenbergMarquardtOptimizer())
        val optimum = squaresSolver.solve()

        val centerPoint :DoubleArray= optimum.point.toArray()
        val pointValueX : Double = centerPoint[0]
        val pointValueY : Double = centerPoint[1]

        val locationRecord = LocationRecord(LOCATION_BEACON_ID, pointValueX, pointValueY, 0.0, 0.0f, LocationProviderType.BEACONS, DateTime.now(), false)

        for (entry in beacons.entries) {
            locationRecord.addProvider(entry.key, PointF(entry.value.getX().toFloat(), entry.value.getY().toFloat()))
        }
        return locationRecord
    }

    @Nullable
    fun getBeaconStuff(item: ConfigurableDevicesScanner.ScanResultItem, id: String): BeaconDeviceInfo? {
        val distance = Utils.computeAccuracy(item)
        if (java.lang.Double.isNaN(distance)) {
            return null
        }
        val beacon = BeaconDeviceInfo(id)
        var distanceSum = beacon.getElementsSum()

        val size = beacon.queue.size
        if (size == MEASUREMENT_COUNT) {
            distanceSum -= beacon.queue.poll()
        }
        beacon.queue.add(distance)
        distanceSum += distance
        beacon.getElementsSum()
        beacon.getCount()
        return beacon
    }
}