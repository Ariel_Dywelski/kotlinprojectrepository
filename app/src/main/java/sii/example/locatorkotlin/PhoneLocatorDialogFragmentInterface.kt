package sii.example.locatorkotlin

/**
 * Created by ariel_dywelski on 09/10/2017.
 */
interface PhoneLocatorDialogFragmentInterface {
    fun doPositiveClick()
    fun doNegativeClick()
    fun finishApplicationClick()
}