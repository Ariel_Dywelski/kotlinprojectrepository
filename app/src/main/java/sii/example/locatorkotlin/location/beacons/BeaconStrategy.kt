package sii.example.locatorkotlin.location.beacons

import android.content.Context
import android.support.v4.util.ArrayMap
import android.util.Log
import com.estimote.sdk.connection.DeviceConnection
import com.estimote.sdk.connection.DeviceConnectionCallback
import com.estimote.sdk.connection.DeviceConnectionProvider
import com.estimote.sdk.connection.exceptions.DeviceConnectionException
import com.estimote.sdk.connection.scanner.ConfigurableDevicesScanner
import com.estimote.sdk.connection.scanner.DeviceType
import com.estimote.sdk.connection.settings.SettingCallback
import sii.example.locatorkotlin.BeaconDeviceInfo
import sii.example.locatorkotlin.location.LocationProviderType
import sii.example.locatorkotlin.location.LocationRecord
import sii.example.locatorkotlin.location.LocationStrategy


/**
 * Created by ariel_dywelski on 09/10/2017.
 */
class BeaconStrategy(val context: Context) : LocationStrategy {

    val DEVICE_CONNECTION: String = "MyDeviceConnection"
    val BEACONS: String = "BEACONS"
    val SPLITTER: String = ","
    val beaconsMap: MutableMap<String, BeaconDeviceInfo> = ArrayMap()

    private var isConnectedToProvider: Boolean = false
    private lateinit var scannerCallback: ConfigurableDevicesScanner.ScannerCallback
    private lateinit var devicesScanner: ConfigurableDevicesScanner
    private lateinit var connectionProvider: DeviceConnectionProvider
    private var lastKnownLocationRecord: LocationRecord? = null

    override fun init() {
        isConnectedToProvider = false
        devicesScanner = ConfigurableDevicesScanner(context)
        connectionProvider = DeviceConnectionProvider(context)

        scannerCallback = ConfigurableDevicesScanner.ScannerCallback { devices ->
            if (devices.isEmpty()) {
                Log.d(BEACONS, "Not beacons found in range. Device position calculate with using GPS.")
            }
            handleBeacons(devices)
        }

        connectionProvider.connectToService {
            isConnectedToProvider = true
            devicesScanner.scanForDevices(scannerCallback)
            devicesScanner.setDeviceTypes(DeviceType.LOCATION_BEACON)
            Log.d(DEVICE_CONNECTION, "Success connection to beacons.")
        }
    }

    private fun handleBeacons(beaconList: List<ConfigurableDevicesScanner.ScanResultItem>) {
        if (beaconList.isEmpty()) {
            return
        }

        devicesScanner.isOwnDevicesFiltering = true
        devicesScanner.setDeviceTypes(DeviceType.LOCATION_BEACON)
        val beaconHelper = BeaconHelper()

        for (i in 0 until beaconList.size) {
            val device = beaconList[i].device
            val deviceID = device.deviceId.toHexString()

            if (beaconsMap.containsKey(deviceID) === false) {
                run {
                    val beacon = beaconHelper.getBeaconStuff(beaconList[i], deviceID)
                    if (beacon != null) {
                        beaconsMap.put(deviceID, beacon)

                        if (isConnectedToProvider) {
                            val connection = connectionProvider.getConnection(device)
                            connectToBeacon(connection, deviceID)
                        }
                    }
                }
            }
        }
        if (beaconList.size > 2) {
            lastKnownLocationRecord = beaconHelper.calculateDevicePosition(beaconsMap)
        } else {
            lastKnownLocationRecord = LocationRecord.getUnavailableRecord(LocationProviderType.BEACONS)
        }
    }

    private fun connectToBeacon(connection: DeviceConnection, id: String) {
        connection.connect(object : DeviceConnectionCallback {
            override fun onConnected() {
                Log.d(DEVICE_CONNECTION, "Connected")
                if (connection.isConnected && connection.settings.deviceInfo.tags().isAvailable) {
                    getTagsFromBeacon(connection, id)
                }
            }

            override fun onConnectionFailed(exception: DeviceConnectionException) {
                Log.d(DEVICE_CONNECTION, "Connection Failed: " + exception.message)
            }

            override fun onDisconnected() {
                Log.d(DEVICE_CONNECTION, "Disconnected")
            }

        })
    }

    private fun getTagsFromBeacon(connection: DeviceConnection, id: String) {
        connection.settings.deviceInfo.tags().get(object : SettingCallback<Set<String>> {

            override fun onSuccess(beaconsTag: Set<String>) {
                if (beaconsTag.size == 1) {
                    setBeaconsCoordinates(beaconsTag, id)
                }
                connection.destroy()
            }

            override fun onFailure(exception: DeviceConnectionException) {
                Log.d(DEVICE_CONNECTION, "TAGS Failed" + exception.message)
                connection.destroy()
            }
        })
    }

    private fun setBeaconsCoordinates(strings: Set<String>, id: String) {
        val beaconValueX : Double
        val beaconValueY : Double
        val beaconTag = strings.iterator().next()
        val split = beaconTag.split(SPLITTER.toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

        beaconValueX = java.lang.Double.parseDouble(split[0])
        beaconValueY = java.lang.Double.parseDouble(split[1])

        beaconsMap[id]?.setX(beaconValueX)
        beaconsMap[id]?.setY(beaconValueY)
    }

    override fun getCurrentLocation(): LocationRecord {
        return if (lastKnownLocationRecord == null) LocationRecord.getUnavailableRecord(LocationProviderType.BEACONS) else lastKnownLocationRecord!!
    }


    override fun destroy() {
        isConnectedToProvider = false
        devicesScanner.stopScanning()
        connectionProvider.destroy()
    }
}