package sii.example.locatorkotlin

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.PointF
import android.graphics.Rect
import android.os.Build
import android.util.AttributeSet
import android.view.View

/**
 * Created by ariel_dywelski on 09/10/2017.
 */
class PhonePositionMapView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {
    init {
        init()
    }


    val RADIUS: Int = 30

    private var distanceCountX: Int = 0
    private var distanceCountY: Int = 0
    private var singleDistanceEntityX: Float = 0.0f
    private var singleDistanceEntityY: Float = 0.0f
    private lateinit var providers: MutableMap<String, PointF>

    private var squareShape: Rect = Rect()
    private var backgroundPaint: Paint = Paint()
    private var gridLinePaint: Paint = Paint()
    private var devicePaint: Paint = Paint()
    private var beaconPaint: Paint = Paint()
    private var devicePosition: PointF = PointF()

    fun updatePosition(x: Double, y: Double) {
        devicePosition.x = x.toFloat()
        devicePosition.y = y.toFloat()
        setResourcesColor()
        postInvalidate()
    }

    fun updateBeaconPosition(beacons: MutableMap<String, PointF>) {
        providers = HashMap()
        if (!beacons.isEmpty()) {
            providers = beacons
        }
        prepareValueToDrawOfficeGrid()
        postInvalidate()
    }

    private fun prepareValueToDrawOfficeGrid() {
        var gridXMaxValue: Double = Double.MIN_VALUE
        var gridXMinValue: Double = Double.MAX_VALUE
        var gridYMaxValue: Double = Double.MIN_VALUE
        var gridYMinValue: Double = Double.MAX_VALUE

        for (entry in providers.entries) {
            val x = entry.value.x
            val y = entry.value.y

            if (x >= gridXMaxValue) {
                gridXMaxValue = x.toDouble()
            }
            if (x <= gridXMinValue) {
                gridXMinValue = x.toDouble()
            }
            if (y >= gridYMaxValue) {
                gridYMaxValue = y.toDouble()
            }
            if (y <= gridYMinValue) {
                gridYMinValue = y.toDouble()
            }
        }
        val distanceX: Float = Math.abs(gridXMaxValue - gridXMinValue).toFloat()
        val distanceY: Float = Math.abs(gridYMaxValue - gridYMinValue).toFloat()

        if (distanceX > 0 && distanceY > 0) {
            distanceCountX = (distanceX + 2).toInt()
            distanceCountY = (distanceY + 2).toInt()

            singleDistanceEntityX = (measuredWidth / distanceCountX).toFloat()
            singleDistanceEntityY = (measuredWidth / distanceCountY).toFloat()
        }
    }

    private fun init() {
        setWillNotDraw(false)
        backgroundPaint = Paint()
        gridLinePaint = Paint()
        devicePaint = Paint()
        beaconPaint = Paint()
        setResourcesColor()
        providers = HashMap()
        devicePosition = PointF()
    }

    private fun setResourcesColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            backgroundPaint.color = resources.getColor(R.color.background, context.theme)
            gridLinePaint.color = resources.getColor(R.color.grid, context.theme)
            devicePaint.color = resources.getColor(R.color.devices, context.theme)
            beaconPaint.color = resources.getColor(R.color.yellow, context.theme)
        } else {
            backgroundPaint.color = resources.getColor(R.color.background)
            gridLinePaint.color = resources.getColor(R.color.grid)
            devicePaint.color = resources.getColor(R.color.devices)
            beaconPaint.color = resources.getColor(R.color.yellow)
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val width: Int = measuredWidth
        val height: Int = measuredHeight

        if (width != height) {
            setMeasuredDimension(width, width)
        }

        if (squareShape == null && width != 0) {
            squareShape = Rect(0, 0, width, width)
        }
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        canvas?.drawRect(squareShape, backgroundPaint)
        canvas?.drawCircle(devicePosition.x * singleDistanceEntityX + singleDistanceEntityX, devicePosition.y * singleDistanceEntityY + singleDistanceEntityY, RADIUS.toFloat(), devicePaint)

        for (i in 0 until distanceCountX) {
            canvas?.drawLine(0F, i * singleDistanceEntityX, measuredWidth.toFloat(), i * singleDistanceEntityX, gridLinePaint)
        }

        for (i in 0 until distanceCountY) {
            canvas?.drawLine(i * singleDistanceEntityY, 0F, i * singleDistanceEntityY, measuredWidth.toFloat(), gridLinePaint)
        }

        for (entry in providers.entries) {
            val x = entry.value.x
            val y = entry.value.y

            canvas?.drawCircle(x * singleDistanceEntityX + singleDistanceEntityX, y * singleDistanceEntityY + singleDistanceEntityY, RADIUS.toFloat(), beaconPaint)
        }
    }
}