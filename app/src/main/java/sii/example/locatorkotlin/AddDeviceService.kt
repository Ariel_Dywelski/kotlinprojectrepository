package sii.example.locatorkotlin

import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.IBinder
import android.provider.Settings.Secure
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import android.widget.Toast
import org.jetbrains.annotations.Nullable
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import sii.example.locatorkotlin.dtos.DeviceDTO
import sii.example.locatorkotlin.services.WebServiceClient
import javax.inject.Inject

/**
 * Created by ariel_dywelski on 09/10/2017.
 */
class AddDeviceService : Service() {

    @Inject lateinit var context: Context
    @Inject lateinit var webServiceClient : WebServiceClient

    val PREFERENCES_DEVICE_ID_KEY : String = "deviceKey"
    val ANDROID_VERSION : String = "Android version: "
    val SHARED_PREFERENCES_DEVICE_ID : String = "deviceId.txt"
    val ERROR_REQUEST : String = "Error request"
    val FAILURE_AUTHORIZATION_TAG : String = "failureAuthorization"
    val FAILURE_ADDING_TAG : String = "failureAdding"
    val INTENT_MESSAGE : String = "message"
    val CORRECT_ADDING_EXTRA : String = "correctAdding"

    override fun onCreate() {
        ((application as PhoneLocatorApplication)).getAppComponent().inject(this)
        super.onCreate()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val androidDeviceId : String = Secure.getString(applicationContext.contentResolver, Secure.ANDROID_ID)
        saveDeviceIdInStorage(androidDeviceId)
        if (getDeviceIdFromStorage() == "No value") {
            saveDeviceIdInStorage(androidDeviceId)
        }

        val deviceName = Build.MANUFACTURER + ": " + Build.MODEL
        val androidVersion = ANDROID_VERSION + Build.VERSION.RELEASE
        addDeviceToDatabase(androidDeviceId, deviceName, androidVersion)
        sendBroadcast(true)
        deviceAuthorization()

        return START_NOT_STICKY
    }

    private fun saveDeviceIdInStorage (deviceId : String){
        val sharedPreferences : SharedPreferences = getSharedPreferences(SHARED_PREFERENCES_DEVICE_ID, Context.MODE_PRIVATE)
        val editor : SharedPreferences.Editor = sharedPreferences.edit()
        editor.putString(PREFERENCES_DEVICE_ID_KEY, deviceId)
        editor.apply()
    }

    private fun getDeviceIdFromStorage() : String {
        val sharedPreferences : SharedPreferences = getSharedPreferences(SHARED_PREFERENCES_DEVICE_ID, Context.MODE_PRIVATE)
        return sharedPreferences.getString(PREFERENCES_DEVICE_ID_KEY, "No value")
    }

    private fun deviceAuthorization(){
        webServiceClient.getAuthorization().enqueue(object : Callback<Void> {

            override fun onResponse(call: Call<Void>?, response: Response<Void>?) {
                if (response != null) {
                    if (!response.isSuccessful) {
                        Toast.makeText(context, "Incorrect Request", Toast.LENGTH_LONG).show()
                    }
                } else {
                    Toast.makeText(context, "Correct authorization", Toast.LENGTH_LONG).show()
                }
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                webServiceClient.getAuthorization().enqueue(object : Callback<Void> {

                    override fun onResponse(call: Call<Void>?, response: Response<Void>?) {
                        if (response != null) {
                            if (!response.isSuccessful) {
                                Toast.makeText(context, "Incorrect request", Toast.LENGTH_LONG).show()
                            }
                        } else {
                            Toast.makeText(context, "Correct authorization", Toast.LENGTH_LONG).show()
                            }
                    }

                    override fun onFailure(call: Call<Void>, t: Throwable) {
                        Log.d(ERROR_REQUEST, t.message)
                        val failureAdding = Intent(context, MainActivity::class.java)
                        failureAdding.putExtra(FAILURE_AUTHORIZATION_TAG, 456)
                        failureAdding.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(failureAdding)
                    }
                })
            }
        })
    }

    @Nullable
    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    private fun addDeviceToDatabase(deviceId: String, deviceName: String, androidVersion: String){
        val deviceToAdd = DeviceDTO(deviceId, deviceName, androidVersion)

        webServiceClient.addDevice(deviceToAdd).enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>?, response: Response<Void>?) {
                if (response != null) {
                    if (!response.isSuccessful) {
                        Toast.makeText(context, "Incorrect request", Toast.LENGTH_LONG).show()
                    }
                }
                else {
                    Toast.makeText(context, "Correct connect to database", Toast.LENGTH_LONG).show()
                }
            }

            override fun onFailure(call: Call<Void>?, t: Throwable?) {
                Log.e(ERROR_REQUEST, t?.message)

                webServiceClient.addDevice(deviceToAdd).enqueue(object : Callback<Void> {
                    override fun onResponse(call: Call<Void>, response: Response<Void>) {
                        if (!response.isSuccessful) {
                            Toast.makeText(context, "Incorrect request", Toast.LENGTH_LONG).show()
                        }
                    }
                    override fun onFailure(call: Call<Void>?, t: Throwable?) {
                        Log.d(ERROR_REQUEST, t?.message)

                        val failureAdding = Intent(context, MainActivity::class.java)
                        failureAdding.putExtra(FAILURE_ADDING_TAG, 123)
                        startActivity(failureAdding)
                    }

                })
            }

        })
    }

    private fun sendBroadcast(successAdding: Boolean){
        val successIntent = Intent(INTENT_MESSAGE)
        successIntent.putExtra(CORRECT_ADDING_EXTRA, successAdding)
        LocalBroadcastManager.getInstance(context).sendBroadcast(successIntent)
    }
}