package sii.example.locatorkotlin

import java.util.*

/**
 * Created by ariel_dywelski on 09/10/2017.
 */
class BeaconDeviceInfo(var id: String) {
    private var elementsSum: Double = 0.toDouble()
    val queue: Queue<Double> = LinkedList()

    private var count: Int = 0
    private var x = 0.0
    private var y = 0.0

    fun getElementsSum(): Double {
        return elementsSum
    }

    fun setElementsSum(elementsSum: Double) {
        this.elementsSum = elementsSum
    }

    fun getCount(): Int {
        return count
    }

    fun setCount() {
        this.count = queue.size
    }

    fun getX(): Double {
        return x
    }

    fun setX(x: Double) {
        this.x = x
    }

    fun getY(): Double {
        return y
    }

    fun setY(y: Double) {
        this.y = y
    }

}

