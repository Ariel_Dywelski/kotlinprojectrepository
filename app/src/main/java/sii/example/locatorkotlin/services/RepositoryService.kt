package sii.example.locatorkotlin.services

import org.joda.time.DateTime
import sii.example.locatorkotlin.location.LocationRecord

/**
 * Created by ariel_dywelski on 06/10/2017.
 */
interface RepositoryService {

    fun getLocationHistory(startDate: DateTime, endDateTime: DateTime): List<LocationRecord>
    fun saveLocation(locationRecord: LocationRecord): Boolean
    fun getLastNotSentLocationRecordList(locationRecordList: List<LocationRecord>) : List<LocationRecord>
}