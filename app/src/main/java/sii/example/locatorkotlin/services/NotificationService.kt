package sii.example.locatorkotlin.services

import sii.example.locatorkotlin.PushMessage

/**
 * Created by ariel_dywelski on 06/10/2017.
 */
interface NotificationService {
    fun handlePushNotification(pushMessageNotification: PushMessage)
    fun displayNotification(title: String, message: String)
}