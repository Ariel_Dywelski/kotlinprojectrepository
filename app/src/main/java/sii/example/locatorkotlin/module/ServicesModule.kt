package sii.example.locatorkotlin.module

import android.content.Context
import dagger.Module
import dagger.Provides
import io.realm.Realm
import sii.example.locatorkotlin.location.LocationStrategyFactory
import sii.example.locatorkotlin.location.LocationStrategyFactoryImpl
import sii.example.locatorkotlin.services.NotificationService
import sii.example.locatorkotlin.services.RepositoryService
import sii.example.locatorkotlin.services.impl.LocationProviderServiceImpl
import sii.example.locatorkotlin.services.impl.NotificationServiceImpl
import sii.example.locatorkotlin.services.impl.RepositoryServiceImpl
import javax.inject.Singleton

/**
 * Created by ariel_dywelski on 09/10/2017.
 */
@Module
class ServicesModule(val context: Context) {

    @Singleton
    @Provides
    fun provideApplicationContext():Context{
        return context
    }
    @Singleton
    @Provides
    fun provideNotificationServices(context: Context) : NotificationService{
        return NotificationServiceImpl(context)
    }

    @Singleton
    @Provides
    fun provideRepositoryService() : RepositoryService {
        return RepositoryServiceImpl()
    }

    @Singleton
    @Provides
    fun provideLocationProvider(repositoryService: RepositoryService, locationStrategyFactory: LocationStrategyFactory): LocationProviderServiceImpl{
        return LocationProviderServiceImpl(repositoryService, locationStrategyFactory)
    }

    @Singleton
    @Provides
    fun provideLocationStrategyFactory(context: Context): LocationStrategyFactory{
        return LocationStrategyFactoryImpl(context)
    }

    @Provides
    fun provideRealmDatabase(): Realm {
        return Realm.getDefaultInstance()
    }
}