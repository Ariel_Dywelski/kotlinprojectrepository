package sii.example.locatorkotlin

import android.app.Application
import com.estimote.sdk.EstimoteSDK
import io.realm.Realm
import io.realm.RealmConfiguration
import sii.example.locatorkotlin.components.AppComponent
import sii.example.locatorkotlin.components.DaggerAppStubComponent
import sii.example.locatorkotlin.module.AppModule
import sii.example.locatorkotlin.module.NetworkingModule
import sii.example.locatorkotlin.module.ServicesModule

/**
 * Created by ariel_dywelski on 06/10/2017.
 */
class PhoneLocatorApplication : Application() {

    val APP_ID : String = "phonelocationtrucker-fgm"
    val APP_TOKEN : String = "20ea17cbc72a48f5ca4f6043620c4a7b"
    val REALM_DATABASE_VERSION : Long = 1

    private lateinit var component: AppComponent

    fun getAppComponent(): AppComponent {
        return component
    }

    override fun onCreate() {
        super.onCreate()

        component = DaggerAppStubComponent
                .builder()
                .appModule(AppModule(this))
                .networkingModule(NetworkingModule())
                .servicesModule(ServicesModule(this))
                .build()

        component.inject(this)

        EstimoteSDK.initialize(applicationContext, APP_ID, APP_TOKEN)
        EstimoteSDK.enableDebugLogging(true)

        createRealmDatabase()
    }

    private fun createRealmDatabase(){
        Realm.init(this)
        val realmConfiguration = RealmConfiguration.Builder()
                .schemaVersion(REALM_DATABASE_VERSION)
                .deleteRealmIfMigrationNeeded()
                .build()
        Realm.setDefaultConfiguration(realmConfiguration)
    }
}