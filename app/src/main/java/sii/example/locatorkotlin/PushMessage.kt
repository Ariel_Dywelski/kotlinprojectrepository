package sii.example.locatorkotlin

/**
 * Created by ariel_dywelski on 06/10/2017.
 */
data class PushMessage(val payload: String,
                       val requestCode: Int,
                       val data: MutableMap<String, String>)