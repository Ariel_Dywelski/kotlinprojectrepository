package sii.example.locatorkotlin

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.DialogFragment
import org.jetbrains.annotations.Nullable



/**
 * Created by ariel_dywelski on 09/10/2017.
 */
class PhoneLocatorDialogFragment : DialogFragment() {

    val ARGUMENTS_TITLE : String = "title"
    val ARGUMENTS_MESSAGE : String = "message"
    val ARGUMENTS_TAG : String = "tag"
    val TAG_PERMISSION : String = "Permission"
    val TAG_FAILURE : String = "Failure"

    lateinit var dialogContext : PhoneLocatorDialogFragmentInterface

    fun newInstance(title: String, message: String, tag: String): PhoneLocatorDialogFragment {
        val fragment = PhoneLocatorDialogFragment()
        val arguments = Bundle()
        arguments.putString(ARGUMENTS_TITLE, title)
        arguments.putString(ARGUMENTS_MESSAGE, message)
        arguments.putString(ARGUMENTS_TAG, tag)
        fragment.arguments = arguments
        return fragment
    }

    @Nullable
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val title : String = arguments.getString(ARGUMENTS_TITLE)
        val message : String = arguments.getString(ARGUMENTS_MESSAGE)
        val tag : String = arguments.getString(ARGUMENTS_TAG)
        return createPermissionDialog(title,message,tag)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is PhoneLocatorDialogFragmentInterface) {
            dialogContext = context
        } else {
            throw IllegalStateException()
        }
    }

    private fun createPermissionDialog(title : String, message : String, tag : String) : Dialog {
        return AlertDialog.Builder(activity)
                .setIcon(R.mipmap.ic_launcher)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", DialogInterface.OnClickListener{ _, _ ->
                    if (tag == TAG_PERMISSION) {
                        dialogContext.doPositiveClick()
                    } else if (tag == TAG_FAILURE) {
                        dialogContext.finishApplicationClick()
                    }
                })
                .setNegativeButton("Cancel" , DialogInterface.OnClickListener { _, _ ->
                    dialogContext.doNegativeClick() })
                .create()
    }
}