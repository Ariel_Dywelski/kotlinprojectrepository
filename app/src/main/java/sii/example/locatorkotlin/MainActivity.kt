package sii.example.locatorkotlin

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.PointF
import android.os.Build
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.estimote.sdk.SystemRequirementsChecker
import org.joda.time.Duration
import sii.example.locatorkotlin.location.LocationProviderType
import sii.example.locatorkotlin.location.LocationRecord
import sii.example.locatorkotlin.location.PhoneLocationObserver
import sii.example.locatorkotlin.services.LocationProviderService
import sii.example.locatorkotlin.services.NotificationService
import java.util.*

class MainActivity : AppCompatActivity(), PhoneLocatorDialogFragmentInterface {

    private val LOCATION_REQUEST: Int = 123
    private val TAG: String = "DeviceConnected"
    private val DEVICE_LOCATOR: String = "Device Locator"
    val PERMISSION_GRANTED: String = "Permission Granted :D"
    val FRAGMENT_ALERT_DIALOG_TAG: String = "FragmentAlertDialog"
    val FAILURE_CONNECTION_DIALOG_TITLE: String = "Failure connection"
    val FAILURE_ADDING_EXTRA: String = "failureAdding"
    val FAILURE_ADDING_DIALOG_MESSAGE: String = "Failure adding device to cloud database"
    val FAILURE_DIALOG_TAG: String = "Failure"
    val FAILURE_CONNECTION_DIALOG_MESSAGE: String = "Failure connection to database"
    val PERMISSION_DIALOG_TITLE: String = "Phone Locator"
    val PERMISSION_DIALOG_MESSAGE: String = "Please give the permission"
    val PERMISSION_DIALOG_TAG: String = "Permission"
    val LOCATION_TEXT_FORMAT: String = "%.2f"
    val UPDATE_POSITION: String = "updatePosition"

    private lateinit var phonePositionMap: PhonePositionMapView
    private lateinit var phoneLatitudeText: TextView
    private lateinit var phoneLongitudeText: TextView
    lateinit var mainLayout: View
    private lateinit var locationProvider : LocationProviderService
    private lateinit var notificationService : NotificationService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ((application as PhoneLocatorApplication)).getAppComponent().inject(this)

        phonePositionMap = findViewById(R.id.locationSquareView)
        phoneLatitudeText = findViewById(R.id.phoneLatitude)
        phoneLongitudeText = findViewById(R.id.phoneLongitude)
        mainLayout = findViewById(R.id.mainLayout)

        handlePermission()

        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, IntentFilter("message"))

        val addServiceIntent = Intent(this, AddDeviceService::class.java)
        startService(addServiceIntent)
    }

        override fun onStart() {
            super.onStart()
            if (!checkNecessaryPermission()) {
                handlePermission()
            }
        }

        override fun onDestroy() {
            super.onDestroy()
            locationProvider.destroy()
            LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
        }

    private val broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val correctAdding = intent?.getBooleanExtra("correctAdding", false)
            if (correctAdding == true) {
                val serviceIntent = Intent(this@MainActivity, SendLocationService::class.java)
                serviceIntent.action = UPDATE_POSITION
                startService(serviceIntent)
            } else {
                failureAuthorizationAlertDialog()
                failureAddingDeviceAlertDialog()
            }
        }
    }

    private fun failureAddingDeviceAlertDialog() {
        val failureAdding: Int = intent.getIntExtra(FAILURE_ADDING_EXTRA, 0)
        if (failureAdding == 123) {
            showAlertDialog(FAILURE_CONNECTION_DIALOG_TITLE, FAILURE_ADDING_DIALOG_MESSAGE, FAILURE_DIALOG_TAG)
        }
    }

    private fun failureAuthorizationAlertDialog() {
        val failureAuthorization: Int = intent.getIntExtra("failureAuthorization", 0)
        if (failureAuthorization == 456) {
            showAlertDialog(FAILURE_CONNECTION_DIALOG_TITLE, FAILURE_CONNECTION_DIALOG_MESSAGE, FAILURE_DIALOG_TAG)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun updateUI(locationRecord: LocationRecord) {
        val latitude: Double = locationRecord.latitude
        val longitude: Double = locationRecord.longitude

        if (locationRecord.providerType == LocationProviderType.BEACONS) {
            val providers: MutableMap<String, PointF> = locationRecord.providers
            if (!providers.isEmpty() && providers.size == 3) {
                phonePositionMap.updateBeaconPosition(providers)
            }
            phoneLatitudeText.text = "Beacon X: ${String.format(Locale.getDefault(), LOCATION_TEXT_FORMAT, latitude)}"
            phoneLongitudeText.text = "Beacon X: ${String.format(Locale.getDefault(), LOCATION_TEXT_FORMAT, longitude)}"
                phonePositionMap.updatePosition(latitude, longitude)

        } else if (locationRecord.providerType == LocationProviderType.FUSED_LOCATION) {
            phoneLatitudeText.text = "Location X: " + (latitude.toString())
            phoneLongitudeText.text = "Location Y: " + (longitude.toString())
        } else {
            phoneLatitudeText.text = "Location X: " + 0.0f
            phoneLongitudeText.text = "Location Y: " + 0.0f
        }
    }
    private fun showAlertDialog(title: String, message: String, tag: String) {
        val fragmentManager: FragmentManager? = supportFragmentManager
        val alertDialog: PhoneLocatorDialogFragment = PhoneLocatorDialogFragment().newInstance(title, message, tag)
        alertDialog.show(fragmentManager, "fragment_alert")
    }

    private fun handlePermission() {
        if (checkNecessaryPermission()) {
//            notificationService.displayNotification(DEVICE_LOCATOR, PERMISSION_GRANTED)
            if (SystemRequirementsChecker.checkWithDefaultDialogs(this)) {
                permissionsGranted()
            }
        }
    }

    private fun permissionsGranted() {
        val duration: Duration = Duration.standardSeconds(5)
        locationProvider.init()
        locationProvider.setUpdateInterval(duration)
        locationProvider.setMinDistanceMovedToUpdate(1.0F)

        locationProvider.addLocationObserver(object : PhoneLocationObserver {
            override fun onLocationChange(locationRecord: LocationRecord) {
                runOnUiThread {
                    updateUI(locationRecord)
                    if (locationRecord.providerType !== LocationProviderType.BEACONS) {
                        val snackbar = Snackbar.make(mainLayout, "You not connect to beacons.", Snackbar.LENGTH_LONG)
                        val snackbarView = snackbar.view
                        val snackbarText = snackbarView.findViewById<TextView>(android.support.design.R.id.snackbar_text) as TextView

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                            snackbarText.textAlignment = View.TEXT_ALIGNMENT_CENTER
                        } else {
                            snackbarText.gravity = Gravity.CENTER_HORIZONTAL
                        }
                        snackbar.show()
                    }
                }
            }

            override fun onLocationNotAvailable() {
                Log.d(TAG, "Location is now unavailable")
            }

        })
    }

    private fun checkNecessaryPermission(): Boolean {
        if (doNotHavePermission(ACCESS_FINE_LOCATION)) {
            if (shouldShowPermissionRationale(ACCESS_FINE_LOCATION) ) {
                showAlertDialog(PERMISSION_DIALOG_TITLE, PERMISSION_DIALOG_MESSAGE, PERMISSION_DIALOG_TAG)
            } else {
                requestLocationPermission()
            }
            return false
        }
        return true
    }

    private fun requestLocationPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(ACCESS_FINE_LOCATION), LOCATION_REQUEST)
    }

    private fun shouldShowPermissionRationale(permission: String): Boolean {
        return ActivityCompat.shouldShowRequestPermissionRationale(this, permission)
    }

    private fun doNotHavePermission(permission: String): Boolean {
        return ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_DENIED
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        handlePermission()
    }
    override fun doPositiveClick() {
        Toast.makeText(this, "OK", Toast.LENGTH_LONG).show()
        requestLocationPermission()
        Log.i(FRAGMENT_ALERT_DIALOG_TAG, "Positive Click")
    }

    override fun doNegativeClick() {
        Toast.makeText(this, "Cancel", Toast.LENGTH_LONG).show()
        Log.i(FRAGMENT_ALERT_DIALOG_TAG, "Negative Click")
    }

    override fun finishApplicationClick() {
        finish()
    }
}
