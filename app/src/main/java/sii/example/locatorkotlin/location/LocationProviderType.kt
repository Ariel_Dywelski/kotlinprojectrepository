package sii.example.locatorkotlin.location

/**
 * Created by ariel_dywelski on 06/10/2017.
 */
enum class LocationProviderType{
    BEACONS,
    FUSED_LOCATION,
    NONE
}