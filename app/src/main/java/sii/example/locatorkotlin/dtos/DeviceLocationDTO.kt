package sii.example.locatorkotlin.dtos

/**
 * Created by ariel_dywelski on 06/10/2017.
 */
data class DeviceLocationDTO (val latitude : Double,
                              val longitude : Double,
                              val battery : Float,
                              val inOffice : Boolean,
                              val timestamp: String,
                              val deviceId: String)