package sii.example.locatorkotlin.location.fused

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.os.Bundle
import android.util.Log
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import org.jetbrains.annotations.NotNull
import sii.example.locatorkotlin.location.LocationProviderType
import sii.example.locatorkotlin.location.LocationRecord
import sii.example.locatorkotlin.location.LocationStrategy



/**
 * Created by ariel_dywelski on 09/10/2017.
 */
class FusedLocationStrategy (val context: Context) : LocationStrategy, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener{

    val FASTEST_INTERVAL : Long = 200
    val INTERVAL : Long = 1000
    val TAG : String = "Fused Location"
    val LOCATION_FUSED_PROVIDER_ID : String = "Location Fused Provider"

    lateinit var googleApiClient : GoogleApiClient
    var lastKnownLocation : LocationRecord? = null

    override fun init() {
        createGoogleApiClient()
        googleApiClient.connect()
    }

    private fun createGoogleApiClient() {
        googleApiClient = GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build()
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Log.d(TAG, "Connection has failed!")
        if (!connectionResult.isSuccess) {
            googleApiClient.disconnect()
        }
    }

    override fun onConnected(@NotNull p0: Bundle?) {
        onConnectToProvider(INTERVAL, FASTEST_INTERVAL, googleApiClient)
    }

    @SuppressLint("MissingPermission")
    private fun onConnectToProvider(interval : Long, fastestInterval : Long, googleApiClient: GoogleApiClient){
        val locationRequest = LocationRequest.create()
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(interval)
                .setFastestInterval(fastestInterval)

        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this)
    }

    override fun onConnectionSuspended(i: Int) {
        Log.d(TAG, "Connection has been suspended")
    }

    override fun getCurrentLocation(): LocationRecord {
        return if (lastKnownLocation == null) LocationRecord.getUnavailableRecord(LocationProviderType.FUSED_LOCATION) else lastKnownLocation!!
    }
    override fun onLocationChanged(location : Location) {
        val altitude = location.altitude
    }

    override fun destroy() {
        googleApiClient.disconnect()
    }
}