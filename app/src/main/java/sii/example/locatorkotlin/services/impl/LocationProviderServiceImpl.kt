package sii.example.locatorkotlin.services.impl

import android.content.Context
import org.joda.time.DateTime
import org.joda.time.Duration
import sii.example.locatorkotlin.location.LocationProviderType
import sii.example.locatorkotlin.location.LocationStrategyFactory
import sii.example.locatorkotlin.location.PhoneLocationObserver
import sii.example.locatorkotlin.services.LocationProviderService
import sii.example.locatorkotlin.services.RepositoryService
import javax.inject.Inject




/**
 * Created by ariel_dywelski on 06/10/2017.
 */
class LocationProviderServiceImpl(repositoryService: RepositoryService, locationStrategyFactory: LocationStrategyFactory) : LocationProviderService {

    @Inject lateinit var context: Context

    private val locationObservers : MutableSet<PhoneLocationObserver> = HashSet()
    private var interval : Duration = Duration.standardSeconds(10)
    private val locationStrategyFactory : LocationStrategyFactory = locationStrategyFactory
    private var isInitialized : Boolean = true
    private lateinit var thread : Thread
    private val runnable : Runnable = Runnable {
        while (true){
            try {
                Thread.sleep(interval.millis)
                if (isInitialized) {
                    val location = locationStrategyFactory.getCurrentLocation()
                    location.dateTime = DateTime.now().toDate()
                    repositoryService.saveLocation(location)

                    for (observer in locationObservers) {
                        observer.onLocationChange(location)
                    }
                }
            }catch (e: InterruptedException){
                break
            }
        }
    }
    override fun init() {
        thread = Thread(runnable)
        if (isInitialized) {
            thread.interrupt()
        }
        locationStrategyFactory.init()
        isInitialized = true
        thread.start()
    }

    override fun destroy() {
        if (thread.isAlive) {
            thread.interrupt()
        }
        locationObservers.clear()
        isInitialized = false
        locationStrategyFactory.destroy()
    }

    override fun addLocationObserver(phoneLocationObserver: PhoneLocationObserver) {
        locationObservers.add(phoneLocationObserver)
    }

    override fun removeLocationObserver(phoneLocationObserver: PhoneLocationObserver) {
        if (locationObservers.contains(phoneLocationObserver)) {
            locationObservers.remove(phoneLocationObserver)
        }
    }

    override fun setUpdateInterval(timeInterval: Duration) {
        interval = timeInterval
    }

    override fun setMinDistanceMovedToUpdate(meters: Float) {
    }

    override fun getBestProviderAvailable(): LocationProviderType {
        return locationStrategyFactory.getCurrentLocation().providerType
    }
}