package sii.example.locatorkotlin.components

import dagger.Component
import sii.example.locatorkotlin.module.AppModule
import sii.example.locatorkotlin.module.NetworkingModule
import sii.example.locatorkotlin.module.ServicesModule
import javax.inject.Singleton

/**
 * Created by ariel_dywelski on 06/10/2017.
 */
@Singleton
@Component(modules = arrayOf(AppModule::class, NetworkingModule::class, ServicesModule::class))
interface AppStubComponent : AppComponent
