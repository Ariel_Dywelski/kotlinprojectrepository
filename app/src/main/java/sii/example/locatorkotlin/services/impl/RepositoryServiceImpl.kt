package sii.example.locatorkotlin.services.impl

import io.realm.Realm
import org.joda.time.DateTime
import sii.example.locatorkotlin.location.LocationRecord
import sii.example.locatorkotlin.services.RepositoryService

/**
 * Created by ariel_dywelski on 06/10/2017.
 */
class RepositoryServiceImpl : RepositoryService {

    private val DATE_EXCEPTION_MESSAGE : String = "Error. Start date is after end date."
    private val QUERY_FIELD_NAME = "dateTime"

    override fun getLocationHistory(startDate: DateTime, endDateTime: DateTime): List<LocationRecord> {
        if (!startDate.isBefore(endDateTime)) {
            throw IllegalArgumentException(DATE_EXCEPTION_MESSAGE)
        }
        val realm = Realm.getDefaultInstance()
        val results = realm.where(LocationRecord::class.java)
                .between(QUERY_FIELD_NAME, startDate.toDate(), endDateTime.toDate())
                .findAll()
        return getLastNotSentLocationRecordList(results)
    }

    override fun saveLocation(locationRecord: LocationRecord): Boolean {
        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        realm.insert(locationRecord)
        realm.commitTransaction()
        realm.close()
        return true
    }

    override fun getLastNotSentLocationRecordList(locationRecordList: List<LocationRecord>): List<LocationRecord> {
        val notsentLocationRecordList : MutableList<LocationRecord> = ArrayList()
        if (!locationRecordList.isEmpty()) {
            for (i in 0 until locationRecordList.size step 1){
                val locationRecord = locationRecordList[i]
                if (!locationRecord.isSent) {
                    notsentLocationRecordList.add(locationRecord)
                }
            }
        }
        return notsentLocationRecordList
    }
}