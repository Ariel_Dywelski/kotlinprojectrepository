package sii.example.locatorkotlin.module

import dagger.Module
import dagger.Provides
import sii.example.locatorkotlin.PhoneLocatorApplication
import javax.inject.Singleton



/**
 * Created by ariel_dywelski on 06/10/2017.
 */
@Module
class AppModule (val application: PhoneLocatorApplication) {

    @Provides
    @Singleton
    fun provideApplication() = application
}