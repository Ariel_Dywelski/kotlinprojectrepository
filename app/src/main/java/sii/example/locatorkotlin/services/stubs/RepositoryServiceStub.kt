package sii.example.locatorkotlin.services.stubs

import org.joda.time.DateTime
import sii.example.locatorkotlin.location.LocationRecord
import sii.example.locatorkotlin.services.RepositoryService

/**
 * Created by ariel_dywelski on 09/10/2017.
 */
class RepositoryServiceStub : RepositoryService {

    val locationHistory : MutableList<LocationRecord> = ArrayList()
    override fun getLocationHistory(startDate: DateTime, endDateTime: DateTime): List<LocationRecord> {
        return emptyList()
    }

    override fun saveLocation(locationRecord: LocationRecord): Boolean {
        locationHistory.add(locationRecord)
        return true
    }

    override fun getLastNotSentLocationRecordList(locationRecordList: List<LocationRecord>): List<LocationRecord> {
        return emptyList()
    }
}