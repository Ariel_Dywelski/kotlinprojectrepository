package sii.example.locatorkotlin.location

import android.content.Context
import sii.example.locatorkotlin.location.beacons.BeaconStrategy
import sii.example.locatorkotlin.location.fused.FusedLocationStrategy

/**
 * Created by ariel_dywelski on 09/10/2017.
 */
class LocationStrategyFactoryImpl(val context: Context) : LocationStrategyFactory{

    lateinit var beaconStrategy : BeaconStrategy
    lateinit var fusedStrategy : FusedLocationStrategy

    override fun init() {
        beaconStrategy = BeaconStrategy(context)
        fusedStrategy = FusedLocationStrategy(context)
        beaconStrategy.init()
        fusedStrategy.init()
    }

    override fun getCurrentLocation(): LocationRecord {
        val location = beaconStrategy.getCurrentLocation()
        if (location.isOffice!!) {
            return location
        }
        return fusedStrategy.getCurrentLocation()
    }

    override fun destroy() {
        beaconStrategy.destroy()
        fusedStrategy.destroy()
    }
}
