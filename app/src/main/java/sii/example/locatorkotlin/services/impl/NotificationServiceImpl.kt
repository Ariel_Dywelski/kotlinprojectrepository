package sii.example.locatorkotlin.services.impl

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.support.v4.app.NotificationCompat
import sii.example.locatorkotlin.MainActivity
import sii.example.locatorkotlin.PushMessage
import sii.example.locatorkotlin.R
import sii.example.locatorkotlin.services.NotificationService



/**
 * Created by ariel_dywelski on 06/10/2017.
 */
class NotificationServiceImpl (val context: Context) : NotificationService {
    val ID : Int = 1234
    val NOTIFICATIONS_TITLE : String = "Notifications"
    val NOTIFICATIONS_MESSAGE : String = "Message"

    override fun handlePushNotification(pushMessageNotification: PushMessage) {
        displayNotification(NOTIFICATIONS_TITLE, pushMessageNotification.data.get(NOTIFICATIONS_MESSAGE)!!)
    }

    override fun displayNotification(title: String, message: String) {
        val notificationManager : NotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val contentPendingIntent = PendingIntent.getActivity(context, 0, Intent(context, MainActivity::class.java), 0)

        val notificationBuilder : NotificationCompat.Builder = NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.common_google_signin_btn_icon_light)
                .setContentIntent(contentPendingIntent)
                .setContentTitle(title)
                .setContentText(message)

        notificationManager.cancelAll()
        notificationManager.notify(ID, notificationBuilder.build())
    }
}