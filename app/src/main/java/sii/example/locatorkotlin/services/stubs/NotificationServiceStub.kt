package sii.example.locatorkotlin.services.stubs

import android.app.NotificationManager
import android.content.Context
import android.support.v4.app.NotificationCompat
import sii.example.locatorkotlin.PushMessage
import sii.example.locatorkotlin.R
import sii.example.locatorkotlin.services.NotificationService

/**
 * Created by ariel_dywelski on 09/10/2017.
 */
class NotificationServiceStub(val context: Context) : NotificationService {
    val ID: Int = 1234
    override fun handlePushNotification(pushMessageNotification: PushMessage) {
        displayNotification("push", pushMessageNotification.payload)
    }

    override fun displayNotification(title: String, message: String) {
        val notificationManager: NotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val notificationBuilder: NotificationCompat.Builder = NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.common_google_signin_btn_icon_light)
                .setContentTitle(title)
                .setContentText(message)

        notificationManager.cancelAll()
        notificationManager.notify(ID, notificationBuilder.build())
    }
}