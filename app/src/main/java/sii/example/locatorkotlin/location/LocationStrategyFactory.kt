package sii.example.locatorkotlin.location

/**
 * Created by ariel_dywelski on 09/10/2017.
 */
interface LocationStrategyFactory {
    fun init()
    fun getCurrentLocation(): LocationRecord
    fun destroy()
}