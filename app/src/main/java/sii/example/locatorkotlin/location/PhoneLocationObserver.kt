package sii.example.locatorkotlin.location

/**
 * Created by ariel_dywelski on 06/10/2017.
 */
interface PhoneLocationObserver {
    fun onLocationChange(locationRecord: LocationRecord)
    fun onLocationNotAvailable()
}