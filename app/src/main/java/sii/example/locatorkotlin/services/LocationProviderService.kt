package sii.example.locatorkotlin.services

import org.joda.time.Duration
import sii.example.locatorkotlin.location.LocationProviderType
import sii.example.locatorkotlin.location.PhoneLocationObserver

/**
 * Created by ariel_dywelski on 06/10/2017.
 */
interface LocationProviderService {
    fun init()
    fun destroy()
    fun addLocationObserver(phoneLocationObserver: PhoneLocationObserver)
    fun removeLocationObserver(phoneLocationObserver: PhoneLocationObserver)
    fun setUpdateInterval(timeInterval: Duration)
    fun setMinDistanceMovedToUpdate(meters: Float)
    fun getBestProviderAvailable(): LocationProviderType
}