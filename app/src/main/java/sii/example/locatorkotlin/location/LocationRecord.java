package sii.example.locatorkotlin.location;

import android.graphics.PointF;

import org.joda.time.DateTime;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;

/**
 * Created by ariel_dywelski on 09/10/2017.
 */

public class LocationRecord extends RealmObject{


    private String id;
    private double latitude;
    private double longitude;
    private double altitude;
    private float accuracy;
    private String enumDescription;
    private Date dateTime;
    private boolean isSentToDatabase;

    @Ignore
    public boolean isOffice;
    @Ignore
    private DateTime time;
    @Ignore
    private Map<String, PointF> providers = new HashMap<>();

    public boolean isSent() {
        return isSentToDatabase;
    }

    public void setSent(boolean isSentToDatabase) {
        this.isSentToDatabase = isSentToDatabase;
    }

    public LocationRecord() {
    }

    public void addProvider(String id, PointF location) {
        providers.put(id, location);
    }

    public static LocationRecord getUnavailableRecord(LocationProviderType locationProviderType) {
        return new LocationRecord(locationProviderType, DateTime.now());
    }

    private LocationRecord(LocationProviderType locationProviderType, DateTime time) {
        enumDescription = locationProviderType.toString();
        this.time = time;
        isOffice = false;
    }

    public LocationRecord(String id, double latitude, double longitude, double altitude, float accuracy, LocationProviderType providerType, DateTime time, boolean isSentToDatabase) {
        isOffice = true;
        this.isSentToDatabase = isSentToDatabase;
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.accuracy = accuracy;
        this.enumDescription = providerType.toString();
        this.time = time;
    }

    public String getId() {
        return id;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public float getAccuracy() {
        return accuracy;
    }

    public DateTime getTime() {
        return time;
    }

    public Map<String, PointF> getProviders() {
        return providers;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public void setAccuracy(float accuracy) {
        this.accuracy = accuracy;
    }

    public void setTime(DateTime time) {
        this.time = time;
    }

    public void setProviders(Map<String, PointF> providers) {
        this.providers = providers;
    }

    public void saveEnum(LocationProviderType locationProviderType) {
        this.enumDescription = locationProviderType.toString();
    }

    public LocationProviderType getProviderType() {
        return (enumDescription != null) ? LocationProviderType.valueOf(enumDescription) : null;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }
}
