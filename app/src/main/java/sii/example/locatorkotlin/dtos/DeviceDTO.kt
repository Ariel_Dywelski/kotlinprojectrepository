package sii.example.locatorkotlin.dtos

/**
 * Created by ariel_dywelski on 06/10/2017.
 */
data class DeviceDTO(val deviceId: String, val deviceName: String, val system : String)