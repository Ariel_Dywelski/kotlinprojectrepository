package sii.example.locatorkotlin

import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import android.os.Build
import android.os.CountDownTimer
import android.os.IBinder
import android.provider.Settings
import android.provider.Settings.Secure.getString
import android.util.Log
import android.widget.Toast
import io.realm.Realm
import io.realm.RealmResults
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import sii.example.locatorkotlin.dtos.DeviceLocationDTO
import sii.example.locatorkotlin.location.LocationProviderType
import sii.example.locatorkotlin.location.LocationRecord
import sii.example.locatorkotlin.services.NotificationService
import sii.example.locatorkotlin.services.RepositoryService
import sii.example.locatorkotlin.services.WebServiceClient
import java.util.*
import javax.inject.Inject


/**
 * Created by ariel_dywelski on 09/10/2017.
 */
class SendLocationService : Service() {

    @Inject lateinit var context: Context
    @Inject lateinit var repositoryService: RepositoryService
    @Inject lateinit var webServiceClient: WebServiceClient
    @Inject lateinit var notificationService: NotificationService

    val MILLIS: Long = 60000
    val ERROR_REQUEST: String = "Error request"
    val UPDATE_POSITION: String = "updatePosition"
    val DATA_FORMAT: String = "yyyy-MM-dd HH:mm:ss"
    val SHARED_PREFERENCES_LAST_DATA_UPDATE: String = "dateTimeLastUpdate.txt"
    val PREFERENCES_DATA_KEY: String = "lastUpdate"
    val COUNT_DOWN_INTERVAL: Long = 5000
    val IS_SENT_TO_DATABASE_FIELD_NAME: String = "isSentTODatabase"

    override fun onCreate() {
        (application as PhoneLocatorApplication).getAppComponent().inject(this)
        super.onCreate()
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onDestroy() {
        notificationService.displayNotification("Destroy notification", "Message to user.")
        super.onDestroy()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null && intent.action == UPDATE_POSITION) {
            object : CountDownTimer(MILLIS, COUNT_DOWN_INTERVAL) {

                override fun onTick(millisUntilFinished: Long) {
                    Log.d(UPDATE_POSITION, UPDATE_POSITION)
                }

                override fun onFinish() {
                    updateLocationToDatabase()
                    start()
                }
            }.start()
        }
        return Service.START_STICKY
    }

    private fun updateLocationToDatabase() {
        val deviceLocationStatusList: MutableList<DeviceLocationDTO> = ArrayList()
        var inOffice = false

        val pattern: String = DATA_FORMAT
        var timestamp: String

        val preferences = getSharedPreferences(SHARED_PREFERENCES_LAST_DATA_UPDATE, Context.MODE_PRIVATE)
        val lastDateUpdate = preferences.getString(PREFERENCES_DATA_KEY, DateTime.now().toString())

        val parseStartDate: DateTime = DateTime.parse(lastDateUpdate)
        val locationHistoryList: List<LocationRecord> = repositoryService.getLocationHistory(parseStartDate, DateTime.now())

        val formatter: DateTimeFormatter = DateTimeFormat.forPattern(pattern)

        val androidDeviceId: String = getString(applicationContext.contentResolver, Settings.Secure.ANDROID_ID)

        for (i in 0 until locationHistoryList.size step 1) {
            val locationRecord: LocationRecord = locationHistoryList[i]
            val latitude: Double = locationRecord.latitude
            val longitude: Double = locationRecord.longitude
            val date: Date = locationRecord.dateTime
            locationRecord.time = DateTime(date)
            val dateTime: DateTime = locationRecord.time
            val locationProviderType: LocationProviderType = locationRecord.providerType
            timestamp = formatter.print(dateTime)
            val isSent: Boolean = locationRecord.isSent
            if (locationProviderType.equals(LocationProviderType.BEACONS)) {
                inOffice = true
            }
            if (!isSent) {
                deviceLocationStatusList.add(DeviceLocationDTO(latitude, longitude, getBatteryLevel(), inOffice, timestamp, androidDeviceId))
            }
        }
        webServiceClient.deviceLocationStatus(deviceLocationStatusList).enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>?, response: Response<Void>?) {
                if (response != null) {
                    if (!response.isSuccessful) {
                        setIsSendingListItems(locationHistoryList)
                        val results: RealmResults<LocationRecord> = getSentLocationRecordList()
                        deleteSentLocationRecord(results)
                        updateLastSentDate()
                    }
                } else {
                    Toast.makeText(context, "Application can not send your location record", Toast.LENGTH_LONG).show()
                }
            }

            override fun onFailure(call: Call<Void>?, t: Throwable?) {
                Log.e(ERROR_REQUEST, t?.message)
            }
        })
    }

    private fun getSentLocationRecordList(): RealmResults<LocationRecord> {
        val realm: Realm = Realm.getDefaultInstance()
        getAllLocationRecordList()
        return realm.where(LocationRecord::class.java).equalTo(IS_SENT_TO_DATABASE_FIELD_NAME, true).findAll()
    }

    private fun getAllLocationRecordList(): RealmResults<LocationRecord> {
        val realm: Realm = Realm.getDefaultInstance()
        return realm.where(LocationRecord::class.java).findAll()
    }

    private fun setIsSendingListItems(locationHistoryList: List<LocationRecord>) {
        val realm: Realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        for (i in 0 until locationHistoryList.size) {
            locationHistoryList[i].isSent = true
        }
        realm.commitTransaction()
        realm.close()
    }

    private fun deleteSentLocationRecord(results: RealmResults<LocationRecord>) {
        val realm = Realm.getDefaultInstance()
        realm.executeTransaction { results.deleteAllFromRealm() }
    }

    private fun updateLastSentDate() {
        val lastUpdateDate = DateTime.now()
        saveLastUpdateDate(lastUpdateDate)
        Toast.makeText(context, "Update location record", Toast.LENGTH_LONG).show()
    }

    private fun saveLastUpdateDate(lastUpdateDate: DateTime) {
        val sharedPreferences = getSharedPreferences(SHARED_PREFERENCES_LAST_DATA_UPDATE, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString(PREFERENCES_DATA_KEY, lastUpdateDate.toString())
        editor.apply()
    }

    private fun getBatteryLevel(): Float {
        val batteryManager: BatteryManager = getSystemService(Context.BATTERY_SERVICE) as BatteryManager
        val batteryLevel: Float

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            batteryLevel = batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY).toFloat()
        } else {
            val batteryIntent: Intent = registerReceiver(null, IntentFilter(Intent.ACTION_BATTERY_CHANGED))!!
            val level: Int = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)
            val scale: Int = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1)

            if (level == -1 || scale == -1) {
                return 50.0f
            }
            batteryLevel = (level / scale).toFloat()
        }
        val batteryLevelResult: Float = batteryLevel / 100
        return batteryLevelResult
    }
}